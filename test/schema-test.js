#!/usr/bin/env node

const fs = require("fs");
const Ajv = require("ajv/dist/2019");
const ajv = new Ajv({strict: false, verbose: false, logger: false});

const schemaData = fs.readFileSync(__dirname + '/../ham.v1.schema-strict.json', 'utf-8');
const schemaObj = JSON.parse(schemaData);
const validator = ajv.compile(schemaObj);
const valid = true
const invalid  = false

validate('empty.json', invalid);
validate('smallest.ham.json', valid);
validate('all.single.ham.json', valid);
validate('three.simple.templates.json', valid);
validate('values.types.json', valid);
validate('links.permutations.json', valid);
validate('links.transclude.not.boolean.fail.json', invalid);
validate('links.rel.null.fail.json', invalid);
validate('links.href.null.fail.json', invalid);
validate('links.name.null.fail.json', invalid);
validate('links.prompt.null.fail.json', invalid);
validate('links.no-rel.json', invalid);
validate('links.no-href.json', invalid);
validate('links.additional.property.fail.json', invalid);
validate('items.entries.no.name.fail.json', invalid)
validate('items.entries.value.permutation.json', valid);
validate('items.entries.value.additional.object.fail.json', invalid);
validate('items.entries.value.array.in.array.fail.json', invalid);
validate('problems.permutations.json', valid);
validate('problems.empty.json', valid);
validate('problems.idref.fail.json', invalid);
validate('problems.idref.null.fail.json', invalid);
validate('problems.category.null.fail.json', invalid);
validate('problems.title.null.fail.json', invalid);
validate('problems.detail.null.fail.json', invalid);
validate('problems.additional.property.fail.json', invalid);
validate('full.ham.json', valid);

function validate(filename, isValid) {
    const valid = validator(JSON.parse(fs.readFileSync(__dirname + '/hams/' + filename, 'utf-8')));
    if ((!valid && isValid) || (valid && !isValid)) {
        console.log('[\x1b[31m%s\x1b[0m] : %s', "fail", filename);
        // if (!valid) { console.log(valid.errors) }
    } else if ((!valid && !isValid) || (valid && isValid)) {
        console.log('[\x1b[33m%s\x1b[0m] : %s', " ok ", filename);
    } else {
        console.log('WTF');
    }
}
